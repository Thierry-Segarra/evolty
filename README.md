# EvoltyFramework

EvoltyFramework est un framework léger et performant pour développer des applications web en PHP. Il offre une structure modulaire, une gestion facile des routes, et des outils intégrés pour simplifier le développement.

## Table des Matières

1. [Introduction](#introduction)
2. [Initialisation](#initialisation)
3. [Configuration](#configuration)
4. [Utilisation](#utilisation)
5. [Structure du Projet](#structure-du-projet)
6. [Détail du Framework](#détail-du-framework)
7. [Détails du Back-end](#détails-du-back-end)
9. [Licence](#licence)

## Introduction

Ce framework MVC (Modèle-Vue-Contrôleur) offre une structure organisée pour la création de projets, adaptée aussi bien aux petits qu'aux grands projets. Il facilite également la création d'un backend RESTful avec un schéma MRC (Modèle-Route-Contrôleur), accompagné d'un explorateur intégré doté de ses propres routeurs et annotations.

### Prérequis

- PHP 7.4 ou supérieur
- Composer

## Initialisation & Configuration

1. Créez votre projet avec cette commande :
    ```sh
    composer create-project evolty/evoltyframework <nom-fichier> dev-main
    ```

2. Mettez à jour les paramètres de configuration dans le fichier `.env` :
    ```env
    PORT_WEB=8000 # View
    PORT_API=8000 # Api

    # Connexion BDD
    BDD_HOST=
    BDD_USERNAME=
    BDD_PASSWORD=
    BDD_DATABASE=
    ```

## Utilisation

### Structure du Projet
    ```
    evoltyframework
    ├── .env
    ├── public
    │   └── index.php 
    │
    ├── public_api
    │   ├── api
    │   │   └── index.php
    │   └── index.php 
    │
    ├── app
    │   ├── Models
    │   │   ├── UserModel.php (Généré par défaut)
    │   │   └── (vos fichiers de modèles)
    │   ├── Controllers
    │   │   ├── UserController.php (Généré par défaut)
    │   │   └── (vos fichiers de contrôleurs)
    │   ├── Routers
    │   │   ├── AuthentificationRouter.php (Généré par défaut)
    │   │   ├── HomeRouter.php (Généré par défaut)
    │   │   ├── HomeApiRouter.php (Généré par défaut)
    │   │   ├── UserApiRouter.php (Généré par défaut)
    │   │   ├── ApiRouter.php (Généré par défaut)
    │   │   ├── ViewRouter.php (Généré par défaut)
    │   │   └── (vos fichiers de routeurs)
    │   └── Views
    │       ├── home.php (Généré par défaut)
    │       └── (vos fichiers de vues)
    ```

## Code exemple MVC

### Models

À chaque création d'un nouveau modèle, il est conseillé de faire appel à `extends Model` car cela permet de faire un CRUD instantanément.
    ```php
    // fonctions par défaut lors de la extends Model
    findOne()
    findAll()
    create()
    update()
    delete()
    ```

    ```php
    namespace App\Models;

    use EvoltyFramework\Tools\Model;
    use \PDO;
    use \PDOException;

    class UserModel extends Model {
        // Insérer toutes les colonnes de votre table
        public function __construct() {
            parent::__construct('user', ['id', 'email', 'username', 'role_id', 'password']);
        }

        // Ajouter d'autres fonctions ici
        public function findByUsername($username) {
            $query = "SELECT * FROM user WHERE username = :username LIMIT 1";
            $stmt = $this->getDbManager()->getDb()->prepare($query);
            $stmt->bindParam(":username", $username);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC); // Ajustez selon votre bibliothèque de base de données
        }
    }
    ```

### Controler

À chaque création d'un nouveau contrôleur, il est conseillé de faire appel à `extends Controller` car cela permet de faire appel à des fonctions par défaut.
    ```php
    // fonctions par défaut lors de la extends Model
    findOne()
    findAll()
    create()
    update()
    delete()
    ```

    ```php
    namespace App\Controllers;

    use App\Models\UserModel;
    use EvoltyFramework\Tools\Controller;

    class UserController extends Controller {
        private $userModel;

        public function __construct(UserModel $userModel) {
            parent::__construct($userModel);
        }
        
        public function createUser($username, $email, $password) {
            if (!$this->validateEmail($email)) {
                return ['erreur' => 'Email non conforme'];
            }
            if (!$this->checkNotEmpty($username) || !$this->checkNotEmpty($password)) {
                return ['erreur' => 'Élément vide'];
            }

            $data = [
                'username' => $username,
                'email' => $email,
                'password' => $this->hashPassword($password),
            ];

            $result = $this->create($data);
            if ($result) {
                return ['result' => 'Utilisateur créé'];
            } else {
                return ['erreur' => 'Problème durant la création de l\'utilisateur'];
            }
        }

        // Ajouter d'autres contrôleurs si besoin
    }
    ```

### Routeur

1. Création de routes
    ```php
    /**
    * @Route
    * - Method: GET
    * - Path: /
    */
    $router->get('/', function ($params) use ($router){
        $view = __DIR__ . '/../Views/home.php';
        // Adding a message parameter to pass to the view
        $params['message'] = "Hello, this is a parameter from the backend!";
        
        $router->renderView($view, $params);
    });
    ```

2. Appeler un nouveau routeur
    Allez dans ViewRouter.php et ajoutez dans ce code pour pouvoir rajouter le routage
    ```php
        require_once($projectRoot.'/app/Routers/<NomRouter>.php');
    ```



## Code exemple Rest API

### Models

À chaque création d'un nouveau modèle, il est conseillé de faire appel à `extends Model` car cela permet de faire un CRUD instantanément.
    ```php
    // fonctions par défaut lors de la extends Model
    findOne()
    findAll()
    create()
    update()
    delete()
    ```

    ```php
    namespace App\Models;

    use EvoltyFramework\Tools\Model;
    use \PDO;
    use \PDOException;

    class UserModel extends Model {
        // Insérer toutes les colonnes de votre table
        public function __construct() {
            parent::__construct('user', ['id', 'email', 'username', 'role_id', 'password']);
        }

        // Ajouter d'autres fonctions ici
        public function findByUsername($username) {
            $query = "SELECT * FROM user WHERE username = :username LIMIT 1";
            $stmt = $this->getDbManager()->getDb()->prepare($query);
            $stmt->bindParam(":username", $username);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC); // Ajustez selon votre bibliothèque de base de données
        }
    }
    ```

### Controler

À chaque création d'un nouveau contrôleur, il est conseillé de faire appel à `extends Controller` car cela permet de faire appel à des fonctions par défaut.
    ```php
    // fonctions par défaut lors de la extends Model
    findOne()
    findAll()
    create()
    update()
    delete()
    ```

    ```php
    namespace App\Controllers;

    use App\Models\UserModel;
    use EvoltyFramework\Tools\Controller;

    class UserController extends Controller {
        private $userModel;

        public function __construct(UserModel $userModel) {
            parent::__construct($userModel);
        }
        
        public function createUser($username, $email, $password) {
            if (!$this->validateEmail($email)) {
                return ['erreur' => 'Email non conforme'];
            }
            if (!$this->checkNotEmpty($username) || !$this->checkNotEmpty($password)) {
                return ['erreur' => 'Élément vide'];
            }

            $data = [
                'username' => $username,
                'email' => $email,
                'password' => $this->hashPassword($password),
            ];

            $result = $this->create($data);
            if ($result) {
                return ['result' => 'Utilisateur créé'];
            } else {
                return ['erreur' => 'Problème durant la création de l\'utilisateur'];
            }
        }

        // Ajouter d'autres contrôleurs si besoin
    }
    ```

### Routeur

1. Groupe de routes
    ```php
    /**
    * @ApiRoute
    * - Groupe: User
    * - Description: Get all users
    */
    $router->addGroupe(function () {
        // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
    });
    ```

2. Création de routes
    ```php
    /**
    * @ApiRoute
    * - Groupe: Home
    * - Method: POST
    * - Path: /testPost
    * - Description: POST example data
    * - Parameters: {
    *     - data: (string) ex:(test) message Post
    * }
    */
    $router->post('/testPost', function ($params) {
        if (empty($params['data'])) {
            http_response_code(400);
            echo json_encode(['error' => 'Data is required']);
        } else {
            echo json_encode(['result' => $params]);
        }
    });
    ```

3. Appeler un nouveau routeur
    Allez dans ApiRouter.php et ajoutez dans ce code pour pouvoir rajouter le routage
    ```php
        require_once($projectRoot.'/app/Routers/<NomRouter>.php');
    ```

## Détails du Back-end

Le dossier "app" regroupe les fichiers pour une utilisation en tant que MVC et MRC :

- `Models` : Contient tous les modèles du projet.
- `Controllers` : Contient tous les contrôleurs du projet.
- `Views` : Contient toutes les vues du projet.
- `Routers` : Contient tous les routeurs du projet.

Le dossier "public" permet un affichage pour une utilisation front-end et back-end.

Le dossier "RestAPI" permet de gérer les appels API pour une utilisation RESTful.

Le dossier "Exploreur" permet de tester les routes pour une utilisation RESTful.

## Licence

Ce projet est sous licence MIT. Voir le fichier `LICENSE` pour plus de détails.
