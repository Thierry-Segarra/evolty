<?php

$projectRoot = dirname(__DIR__, 1);

// Inclure le fichier autoload de Composer
require $projectRoot . '/vendor/autoload.php';

use EvoltyFramework\Routing\ViewRouter;

$router = new ViewRouter();

require_once($projectRoot.'/app/Routers/ViewRouter.php');

// Permet de faire appel a un Explorateur de Route
// $router->APIExplorerRoute();
$router->route();