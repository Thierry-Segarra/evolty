<?php

$projectRoot = dirname(__DIR__, 2);

// Inclure le fichier autoload de Composer
require $projectRoot . '/vendor/autoload.php';

use EvoltyFramework\Routing\ApiRouter;

$router = new ApiRouter();

require_once($projectRoot.'/app/Routers/ApiRouter.php');

$router->route();