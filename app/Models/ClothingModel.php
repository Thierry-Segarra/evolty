<?php

namespace App\Models;

use EvoltyFramework\Tools\Model;
use PDO;
use PDOException;

class ClothingModel extends Model {

    public function __construct() {
        // Call the parent class constructor with the table name and columns
        parent::__construct('clothing', ['id', 'clothing_style_id', 'color', 'clothing_type_id', 'size', 'description', 'brand_id','url']);
    }
}
