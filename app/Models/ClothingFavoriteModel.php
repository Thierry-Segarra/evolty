<?php

namespace App\Models;

use EvoltyFramework\Tools\Model;
use PDO;
use PDOException;

class ClothingFavoriteModel extends Model {

    public function __construct() {
        // Call the parent class constructor with the table name and columns
        parent::__construct('clothing_favorite', ['id', 'user_id', 'clothing_style_id']);
    }

}
