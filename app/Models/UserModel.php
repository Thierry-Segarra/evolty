<?php

namespace App\Models;

use EvoltyFramework\Tools\Model;
use \PDO;
use \PDOException;
class UserModel extends Model {

    public function __construct() {
        // Call the parent class constructor with the table name and columns
        parent::__construct('user', ['id', 'email', 'gender', 'birth_date', 'username', 'role_id','password']);
    }

    public function findByUsername($username) {
        $query = "SELECT * FROM user WHERE username = :username LIMIT 1";
        $stmt = $this->getDbManager()->getDb()->prepare($query);
        $stmt->bindParam(":username", $username);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC); // Adjust based on your database library
    }
}
