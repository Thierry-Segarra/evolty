<?php

namespace App\Models;

use EvoltyFramework\Tools\Model;
use PDO;
use PDOException;

class ClothingTypeModel extends Model {

    public function __construct() {
        // Call the parent class constructor with the table name and columns
        parent::__construct('clothing_type', ['id', 'name', 'category']);
    }

}
