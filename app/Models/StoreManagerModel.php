<?php

namespace App\Models;

use EvoltyFramework\Tools\Model;
use PDO;
use PDOException;

class StoreManagerModel extends Model {

    public function __construct() {
        // Call the parent class constructor with the table name and columns
        parent::__construct('store_manager', ['id', 'store_id', 'user_id', 'role_id']);
    }

}
