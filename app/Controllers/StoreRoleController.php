<?php

namespace App\Controllers;

use App\Models\StoreRoleModel;

use EvoltyFramework\Tools\Controller;

class StoreRoleController extends Controller {

    private $storeRoleModel;

    public function __construct(StoreRoleModel $storeRoleModel) {
        $this->storeRoleModel = $storeRoleModel;
    }

    public function findAll() {
        return $this->storeRoleModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->storeRoleModel->findOne($conditions);
    }
    
    public function createStoreRole($name) {

        if($this->checkNotEmpty($name) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name
        ];

        $result = $this->storeRoleModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateStoreRole($id, $name) {
        $data = [
            'name' => $name,
        ];

        return $this->storeRoleModel->update($id, $data);
    }

    public function deleteStoreRole($id) {
        $this->storeRoleModel->delete($id);
    }

    public function getStoreRoleById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->storeRoleModel->findAll($data);
    }


}
