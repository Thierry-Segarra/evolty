<?php

namespace App\Controllers;

use App\Models\UserModel;

use EvoltyFramework\Tools\Controller;

class UserController extends Controller {

    private $userModel;

    public function __construct(UserModel $userModel) {
        $this->userModel = $userModel;
    }

    public function findAll() {
        return $this->userModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->userModel->findOne($conditions);
    }
    
    public function createUser($username, $email, $password, $gender, $birth_date, $role_id) {

        if($email == false){
            return ['erreur' => 'email non conforme'];
        }
        if($this->checkNotEmpty($username) == false && $this->checkNotEmpty($password) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'username' => $username,
            'email' => $email,
            'password' => $this->hashPassword($password),
            'gender' => $gender,
            'birth_date' => $birth_date,
            'role_id' => $role_id
        ];

        $result = $this->userModel->create($data);
        if ($result) {
            return ['result' => 'Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du User' ];
        }
    }

    public function updateUser($id, $username, $email) {
        $data = [
            'username' => $username,
            'email' => $email
        ];

        return $this->userModel->update($id, $data);
    }

    public function deleteUser($id) {
        $this->userModel->delete($id);
    }

    public function getUserById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->userModel->findAll($data);
    }

    public function login($username, $password) {
        if ($this->checkNotEmpty($username) == false && $this->checkNotEmpty($password)) {

            $user = $this->userModel->findByUsername($username);

            if ($user && $this->verifyPassword($this->hashPassword($password), $user['password'])) {
                // Successful login
                $_SESSION['user_id'] = $user['id'];
                return true;
            } else {
                // Failed login
                return false;
            }
        }
    }

    public function logout() {
        // Unset all session variables
        $_SESSION = array();

        // Destroy the session
        session_destroy();

        // Redirect or perform any other action after logout
        exit();
    }



    public function authenticateUser($username, $password) {
        
        if ($this->checkNotEmpty($username) && $this->checkNotEmpty($password)) {

            
            $user = $this->userModel->findByUsername($username);


            if ($user && $this->verifyPassword($password, $user['password'])) {
                // Successful login
                $auth = [
                    'id' => $user['id'],
                    'username' => $user['username'],
                    'email' => $user['email']
                ];
                return $auth;
            } else {
                // Failed login
                return false;
            }
        }else {
            // Failed login
            return false;
        }
    }

}
