<?php

namespace App\Controllers;

use App\Models\ClothingTypeModel;

use EvoltyFramework\Tools\Controller;

class ClothingTypeController extends Controller {

    private $clothingTypeModel;

    public function __construct(ClothingTypeModel $clothingTypeModel) {
        $this->clothingTypeModel = $clothingTypeModel;
    }

    public function findAll() {
        return $this->clothingTypeModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->clothingTypeModel->findOne($conditions);
    }
    
    public function createClothingType($name,$category) {

        if($this->checkNotEmpty($name) == false ||$this->checkNotEmpty($category) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name,
            'category' => $category
        ];

        $result = $this->clothingTypeModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateClothingType($id, $name, $category) {

        $data = [
            'name' => $name,
            'category' => $category
        ];


        return $this->clothingTypeModel->update($id, $data);
    }

    public function deleteClothingType($id) {
        $this->clothingTypeModel->delete($id);
    }

    public function getClothingTypeById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->clothingTypeModel->findAll($data);
    }


}
