<?php

namespace App\Controllers;

use App\Models\StoreManagerModel;

use EvoltyFramework\Tools\Controller;

class StoreManagerController extends Controller {

    private $storeManagerModel;

    public function __construct(StoreManagerModel $storeManagerModel) {
        $this->storeManagerModel = $storeManagerModel;
    }

    public function findAll() {
        return $this->storeManagerModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->storeManagerModel->findOne($conditions);
    }
    
    public function createStoreManager($store_id,$user_id,$role_id) {

        if($this->checkNotEmpty($store_id) == false||$this->checkNotEmpty($user_id) == false || $this->checkNotEmpty($role_id) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'store_id' => $store_id,
            'user_id' => $user_id,
            'role_id' => $role_id
        ];

        $result = $this->storeManagerModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateStoreManager($id,$store_id,$user_id,$role_id) {
        $data = [
            'store_id' => $store_id,
            'user_id' => $user_id,
            'role_id' => $role_id
        ];


        return $this->storeManagerModel->update($id, $data);
    }

    public function deleteStoreManager($id) {
        $this->storeManagerModel->delete($id);
    }

    public function getStoreManagerById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->storeManagerModel->findAll($data);
    }


}
