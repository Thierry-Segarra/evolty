<?php

namespace App\Controllers;

use App\Models\ClothingModel;

use EvoltyFramework\Tools\Controller;

class ClothingController extends Controller {

    private $clothingModel;

    public function __construct(ClothingModel $clothingModel) {
        $this->clothingModel = $clothingModel;
    }

    public function findAll() {
        return $this->clothingModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->clothingModel->findOne($conditions);
    }
    
    public function createClothing($clothing_style_id, $color, $clothing_type_id, $size, $description, $brand_id, $url) {

        if($this->checkNotEmpty($clothing_style_id) == false ||$this->checkNotEmpty($color) == false ||$this->checkNotEmpty($clothing_type_id) == false || $this->checkNotEmpty($size) == false ||$this->checkNotEmpty($description) == false ||$this->checkNotEmpty($brand_id) == false||$this->checkNotEmpty($url) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'clothing_style_id' => $clothing_style_id,
            'color' => $color,
            'clothing_type_id' => $clothing_type_id,
            'size' => $size,
            'description' => $description,
            'brand_id' => $brand_id,
            'url' => $url,
        ];

        $result = $this->clothingModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateClothing($id, $clothing_style_id, $color, $clothing_type_id, $size, $description, $brand_id) {
        $data = [
            'clothing_style_id' => $clothing_style_id,
            'color' => $color,
            'clothing_type_id' => $clothing_type_id,
            'size' => $size,
            'description' => $description,
            'brand_id' => $brand_id,
        ];

        return $this->clothingModel->update($id, $data);
    }

    public function deleteClothing($id) {
        $this->clothingModel->delete($id);
    }

    public function getClothingById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->clothingModel->findAll($data);
    }


}
