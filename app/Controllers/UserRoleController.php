<?php

namespace App\Controllers;

use App\Models\UserRoleModel;

use EvoltyFramework\Tools\Controller;

class UserRoleController extends Controller {

    private $userRoleModel;

    public function __construct(UserRoleModel $userRoleModel) {
        $this->userRoleModel = $userRoleModel;
    }

    public function findAll() {
        return $this->userRoleModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->userRoleModel->findOne($conditions);
    }
    
    public function createUserRole($name) {

        if($this->checkNotEmpty($name) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name
        ];

        $result = $this->userRoleModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateUserRole($id, $name) {
        $data = [
            'name' => $name,
        ];

        return $this->userRoleModel->update($id, $data);
    }

    public function deleteUserRole($id) {
        $this->userRoleModel->delete($id);
    }

    public function getUserRoleById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->userRoleModel->findAll($data);
    }


}
