<?php

namespace App\Controllers;

use App\Models\ClothingFavoriteModel;

use EvoltyFramework\Tools\Controller;

class ClothingFavoriteController extends Controller {

    private $clothingFavoriteModel;

    public function __construct(ClothingFavoriteModel $clothingFavoriteModel) {
        $this->clothingFavoriteModel = $clothingFavoriteModel;
    }

    public function findAll() {
        return $this->clothingFavoriteModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->clothingFavoriteModel->findOne($conditions);
    }
    
    public function createClothingFavorite($user_id,$clothing_style_id) {

        if($this->checkNotEmpty($user_id) == false ||$this->checkNotEmpty($clothing_style_id) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'user_id' => $user_id,
            'clothing_style_id' => $clothing_style_id
        ];

        $result = $this->clothingFavoriteModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateClothingFavorite($id, $user_id, $clothing_style_id) {

        $data = [
            'user_id' => $user_id,
            'clothing_style_id' => $clothing_style_id
        ];


        return $this->clothingFavoriteModel->update($id, $data);
    }

    public function deleteClothingFavorite($id) {
        $this->clothingFavoriteModel->delete($id);
    }

    public function getClothingFavoriteById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->clothingFavoriteModel->findAll($data);
    }


}
