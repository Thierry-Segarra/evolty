<?php

namespace App\Controllers;

use App\Models\ClothingStyleModel;

use EvoltyFramework\Tools\Controller;

class ClothingStyleController extends Controller {

    private $clothingStyleModel;

    public function __construct(ClothingStyleModel $clothingStyleModel) {
        $this->clothingStyleModel = $clothingStyleModel;
    }

    public function findAll() {
        return $this->clothingStyleModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->clothingStyleModel->findOne($conditions);
    }
    
    public function createClothingStyle($name) {

        if($this->checkNotEmpty($name) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name
        ];

        $result = $this->clothingStyleModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateClothingStyle($id, $name) {
        $data = [
            'name' => $name,
        ];

        return $this->clothingStyleModel->update($id, $data);
    }

    public function deleteClothingStyle($id) {
        $this->clothingStyleModel->delete($id);
    }

    public function getClothingStyleById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->clothingStyleModel->findAll($data);
    }


}
