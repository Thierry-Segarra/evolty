<?php

namespace App\Controllers;

use App\Models\BrandModel;

use EvoltyFramework\Tools\Controller;

class BrandController extends Controller {

    private $brandModel;

    public function __construct(BrandModel $brandModel) {
        $this->brandModel = $brandModel;
    }

    public function findAll() {
        return $this->brandModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->brandModel->findOne($conditions);
    }
    
    public function createBrand($name) {

        if($this->checkNotEmpty($name) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name
        ];

        $result = $this->brandModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateBrand($id, $name) {
        $data = [
            'name' => $name,
        ];

        return $this->brandModel->update($id, $data);
    }

    public function deleteBrand($id) {
        $this->brandModel->delete($id);
    }

    public function getBrandById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->brandModel->findAll($data);
    }


}
