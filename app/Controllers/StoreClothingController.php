<?php

namespace App\Controllers;

use App\Models\StoreClothingModel;

use EvoltyFramework\Tools\Controller;

class StoreClothingController extends Controller {

    private $storeClothingModel;

    public function __construct(StoreClothingModel $storeClothingModel) {
        $this->storeClothingModel = $storeClothingModel;
    }

    public function findAll() {
        return $this->storeClothingModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->storeClothingModel->findOne($conditions);
    }
    
    public function createStoreClothing($store_id,$clothing_id) {

        if($this->checkNotEmpty($store_id) == false ||$this->checkNotEmpty($clothing_id) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'store_id' => $store_id,
            'clothing_id' => $clothing_id
        ];

        $result = $this->storeClothingModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateStoreClothing($id, $store_id, $clothing_id) {

        $data = [
            'store_id' => $store_id,
            'clothing_id' => $clothing_id
        ];


        return $this->storeClothingModel->update($id, $data);
    }

    public function deleteStoreClothing($id) {
        $this->storeClothingModel->delete($id);
    }

    public function getStoreClothingById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->storeClothingModel->findAll($data);
    }


}
