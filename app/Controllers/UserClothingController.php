<?php

namespace App\Controllers;

use App\Models\UserClothingModel;

use EvoltyFramework\Tools\Controller;

class UserClothingController extends Controller {

    private $userClothingModel;

    public function __construct(UserClothingModel $userClothingModel) {
        $this->userClothingModel = $userClothingModel;
    }

    public function findAll() {
        return $this->userClothingModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->userClothingModel->findOne($conditions);
    }
    
    public function createUserClothing($user_id,$clothing_id) {

        if($this->checkNotEmpty($user_id) == false ||$this->checkNotEmpty($clothing_id) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'user_id' => $user_id,
            'clothing_id' => $clothing_id
        ];

        $result = $this->userClothingModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateUserClothing($id, $user_id, $clothing_id) {

        $data = [
            'user_id' => $user_id,
            'clothing_id' => $clothing_id
        ];


        return $this->userClothingModel->update($id, $data);
    }

    public function deleteUserClothing($id) {
        $this->userClothingModel->delete($id);
    }

    public function getUserClothingById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->userClothingModel->findAll($data);
    }


}
