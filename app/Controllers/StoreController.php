<?php

namespace App\Controllers;

use App\Models\StoreModel;

use EvoltyFramework\Tools\Controller;

class StoreController extends Controller {

    private $StoreModel;

    public function __construct(StoreModel $StoreModel) {
        $this->StoreModel = $StoreModel;
    }

    public function findAll() {
        return $this->StoreModel->findAll();
    }
    
    public function findOne($conditions) {
        return $this->StoreModel->findOne($conditions);
    }
    
    public function createStore($name,$address,$phone,$email) {

        if($this->checkNotEmpty($name) == false ||$this->checkNotEmpty($address) == false ||$this->checkNotEmpty($phone) == false ||$this->checkNotEmpty($email) == false){
            return ['erreur' => 'element vide'];
        }


        $data = [
            'name' => $name,
            'address' => $address,
            'phone' => $phone,
            'email' => $email,
        ];

        $result = $this->StoreModel->create($data);
        if ($result) {
            return ['result' => 'Role Utilisateur Crée' ];
        }else{
            return ['erreur' => 'Probleme durant la créeation du Role' ];
        }
    }

    public function updateStore($id,$name,$address,$phone,$email) {
        $data = [
            'name' => $name,
            'address' => $address,
            'phone' => $phone,
            'email' => $email,
        ];

        return $this->StoreModel->update($id, $data);
    }

    public function deleteStore($id) {
        $this->StoreModel->delete($id);
    }

    public function getStoreById($id) {
        $data = [
            'id' => $id
        ];
        // return $data;
        return $this->StoreModel->findAll($data);
    }


}
