<?php
use App\Controllers\IaController;

use EvoltyFramework\Tools\Log;


/**
 * @ApiRoute
 * - Groupe: Ia
 * - Description: Call Ia
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});



/**
 * @ApiRoute
 * - Groupe: Ia
 * - Method: POST
 * - Path: /ia/Callia
 * - Description: POST example data
 * - Parameters: {
 *     - image: (file) ex:(test) message Post
 * }
 */
$router->post('/ia/Callia', function ($params) {
    $log = new Log();
    $log->addArrayLog($params);

    if (!isset($_FILES['image']) || $_FILES['image']['error'] !== UPLOAD_ERR_OK) {
        http_response_code(400);
        echo json_encode(['error' => 'Invalid or missing file']);
        return;
    }

    $image = $_FILES['image'];

    // Assurez-vous que le fichier est une image
    $mimeType = mime_content_type($image['tmp_name']);
    if (!str_starts_with($mimeType, 'image/')) {
        http_response_code(400);
        echo json_encode(['error' => 'Invalid file type']);
        return;
    }

    // Préparez le fichier pour l'envoi au serveur distant
    $formData = [
        'file' => new CURLFile($image['tmp_name'], $mimeType, $image['name'])
    ];

    // URL du serveur distant
    $url = 'http://147.79.117.164:2020/transfere_image/';

    // Utilisez cURL pour envoyer l'image
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $formData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    if ($response === false) {
        http_response_code(500);
        echo json_encode(['error' => 'Failed to connect to the server']);
        return;
    }

    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpCode !== 200) {
        http_response_code($httpCode);
        echo json_encode(['error' => 'Server responded with an error', 'details' => $response]);
        return;
    }
    // Retourner la réponse du serveur distant
    echo $response;
});
