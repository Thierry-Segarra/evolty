<?php
use EvoltyFramework\Tools\Log;
/**
 * @ApiRoute
 * - Groupe: Home
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: Home
 * - Method: GET
 * - Path: /
 */
$router->get('/', function ($params) {
    $result = 'Bienvenue sur la page d\'accueil!';
    echo json_encode(['result' => $result]);
});

/**
 * @ApiRoute
 * - Groupe: Home
 * - Method: GET
 * - Path: /about
 * - Description: Get example data
 */
$router->get('/about', function ($params) {
    $result = 'À propos de nous';
    echo json_encode(['result' => $result]);
});

/**
 * @ApiRoute
 * - Groupe: Home
 * - Method: GET
 * - Path: /testGet
 * - Description: Get example data
 * - Parameters: {
 *     - data: (string) ex:(test) message Get
 * }
 */
$router->get('/testGet', function ($params) {
    if (!$params || $params == null) {
        http_response_code(400); // Bad Request
        echo json_encode(['error' => 'Missing parameters']);
    } else {
        echo json_encode(['result' => $params]);
    }
});

/**
 * @ApiRoute
 * - Groupe: Home
 * - Method: POST
 * - Path: /testPost
 * - Description: POST example data
 * - Parameters: {
 *     - data: (string) ex:(test) message Post
 * }
 */
$router->post('/testPost', function ($params) {
    if (empty($params['data'])) {
        http_response_code(400);
        echo json_encode(['error' => 'Data is required']);
    } else {
        echo json_encode(['result' => $params]);
    }
});

/**
 * @ApiRoute
 * - Groupe: Home
 * - Method: POST
 * - Path: /test2Post
 * - Description: POST example data
 * - Parameters: {
 *     - data: (string) message Post
 * }
 */
$router->post('/test2Post', function ($params) {
    if (empty($params['data'])) {
        http_response_code(400); // Bad Request
        echo json_encode(['error' => 'Data is required']);
    } else {
        $contenu = $params['data'];
        echo json_encode(['result' => $contenu]);
        // Contenu à écrire dans le fichier texte
    }
});

?>
