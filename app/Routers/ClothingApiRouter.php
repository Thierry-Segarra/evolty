<?php
use App\Controllers\ClothingController;
use App\Models\ClothingModel;

// Instantiate the ClothingModel and ClothingController
$clothingModel = new ClothingModel();
$clothingController = new ClothingController($clothingModel);

/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Description: Get all clothing 
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Method: GET
 * - Path: /clothing/findAll
 * - Description: Get all clothing 
 */
$router->get('/clothing/findAll', function ($params) use ($clothingController) {
    $clothingData = $clothingController->findAll();
    echo json_encode(['result' => $clothingData]);
});

// Route for getClothingById
/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Method: POST
 * - Path: /clothing/getClothing
 * - Description: Get clothing  by Id
 * - Parameters: {
 *     - id: (Int) id Clothing
 * }
 */ 
$router->post('/clothing/getClothing', function ($params) use ($clothingController) {
    $clothingId = $params['id'];

    if (isset($clothingId)) {
        $clothingData = $clothingController->getClothingById($clothingId);
        echo json_encode(['result' => $clothingData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createClothing
/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Method: POST
 * - Path: /clothing/createClothing
 * - Description: Create a new clothing 
 * - Parameters: {
 *     - clothing_style_id: (Int) clothing  id
 *     - color: (String) color
 *     - clothing_type_id: (Int) clothing type id
 *     - size: (String) size
 *     - description: (String) description
 *     - brand_id: (Int) brand id
 *     - url: (String) url
 * }
 */
$router->post('/clothing/createClothing', function ($params) use ($clothingController) {
    $clothingId = $params['clothing_style_id'] ?? null;
    $color = $params['color'] ?? null;
    $clothingTypeId = $params['clothing_type_id'] ?? null;
    $size = $params['size'] ?? null;
    $description = $params['description'] ?? null;
    $brandId = $params['brand_id'] ?? null;
    $url = $params['url'] ?? null;

    if ($clothingId && $color && $clothingTypeId && $size && $description && $brandId) {
        echo json_encode($clothingController->createClothing($clothingId, $color, $clothingTypeId, $size, $description, $brandId,$url));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateClothing
/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Method: PUT
 * - Path: /clothing/updateClothing
 * - Description: Update clothing  by Id
 * - Parameters: {
 *     - id: (Int) clothing  id
 *     - clothing_style_id: (Int) clothing  id
 *     - color: (String) color
 *     - clothing_type_id: (Int) clothing type id
 *     - size: (String) size
 *     - description: (String) description
 *     - brand_id: (Int) brand id
 * }
 */
$router->put('/clothing/updateClothing', function ($params) use ($clothingController) {
    $clothingId = $params['id'] ?? null;
    $newClothingId = $params['clothing_style_id'] ?? null;
    $color = $params['color'] ?? null;
    $clothingTypeId = $params['clothing_type_id'] ?? null;
    $size = $params['size'] ?? null;
    $description = $params['description'] ?? null;
    $brandId = $params['brand_id'] ?? null;

    if ($clothingId && $newClothingId && $color && $clothingTypeId && $size && $description && $brandId) {
        $result = $clothingController->updateClothing($clothingId, $newClothingId, $color, $clothingTypeId, $size, $description, $brandId);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteClothing
/**
 * @ApiRoute
 * - Groupe: Clothing
 * - Method: DELETE
 * - Path: /clothing/deleteClothing
 * - Description: Delete clothing  by Id
 * - Parameters: {
 *     - id: (Int) clothing  id
 * }
 */
$router->delete('/clothing/deleteClothing', function ($params) use ($clothingController) {
    $clothingId = $params['id'] ?? null;

    if ($clothingId) {
        $clothingController->deleteClothing($clothingId);
        echo json_encode(['result' => 'Clothing  supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
