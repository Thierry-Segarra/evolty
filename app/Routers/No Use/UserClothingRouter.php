<?php
use App\Controllers\UserClothingController;
use App\Models\UserClothingModel;

// Instantiate the UserClothingModel and UserClothingController
$userClothingModel = new UserClothingModel();
$userClothingController = new UserClothingController($userClothingModel);

/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Description: Get all user clothings
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Method: GET
 * - Path: /userClothing/findAll
 * - Description: Get all user clothings
 */
$router->get($url.'/userClothing/findAll', function ($params) use ($userClothingController){
    $userClothingData = $userClothingController->findAll();
    echo json_encode(['result' => $userClothingData]);
});

// Route for getUserClothingById
/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Method: POST
 * - Path: /userClothing/getUserClothing
 * - Description: Get user clothing by Id
 * - Parameters: {
 *     - id: (Int) id UserClothing
 * }
 */ 
$router->post($url.'/userClothing/getUserClothing', function ($params) use ($userClothingController){
    $userClothingId = $params['id'];

    if (isset($userClothingId) && $userClothingId != null) {
        $userClothingData = $userClothingController->getUserClothingById($userClothingId);
        echo json_encode(['result' => $userClothingData]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for createUserClothing
/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Method: POST
 * - Path: /userClothing/createUserClothing
 * - Description: Create a new user clothing
 * - Parameters: {
 *     - user_id: (Int) user_id
 *     - clothing_id: (Int) clothing_id
 * }
 */
$router->post($url.'/userClothing/createUserClothing', function ($params) use ($userClothingController) {
    $user_id = $params['user_id'] ?? null;
    $clothing_id = $params['clothing_id'] ?? null;

    if ($user_id && $clothing_id) {
        echo json_encode($userClothingController->createUserClothing($user_id, $clothing_id));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateUserClothing
/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Method: PUT
 * - Path: /userClothing/updateUserClothing
 * - Description: Update user clothing by Id
 * - Parameters: {
 *     - id: (Int) id UserClothing
 *     - user_id: (Int) user_id
 *     - clothing_id: (Int) clothing_id
 * }
 */
$router->put($url.'/userClothing/updateUserClothing', function ($params) use ($userClothingController) {
    $userClothingId = $params['id'] ?? null;
    $user_id = $params['user_id'] ?? null;
    $clothing_id = $params['clothing_id'] ?? null;

    if ($userClothingId && $user_id && $clothing_id) {
        $result = $userClothingController->updateUserClothing($userClothingId, $user_id, $clothing_id);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteUserClothing
/**
 * @ApiRoute
 * - Groupe: UserClothing
 * - Method: DELETE
 * - Path: /userClothing/deleteUserClothing
 * - Description: Delete user clothing by Id
 * - Parameters: {
 *     - id: (Int) id UserClothing
 * }
 */
$router->delete($url.'/userClothing/deleteUserClothing', function ($params) use ($userClothingController) {
    $userClothingId = $params['id'] ?? null;

    if ($userClothingId) {
        $userClothingController->deleteUserClothing($userClothingId);
        echo json_encode(['result' => 'Vêtement utilisateur supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});
