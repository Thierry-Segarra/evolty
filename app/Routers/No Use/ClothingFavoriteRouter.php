<?php
use App\Controllers\ClothingFavoriteController;
use App\Models\ClothingFavoriteModel;

// Instantiate the ClothingFavoriteModel and ClothingFavoriteController
$clothingFavoriteModel = new ClothingFavoriteModel();
$clothingFavoriteController = new ClothingFavoriteController($clothingFavoriteModel);


/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Description: Get all clothing 
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Method: GET
 * - Path: /clothingFavorite/findAll
 * - Description: Get all clothing favorites
 */
$router->get($url.'/clothingFavorite/findAll', function ($params) use ($clothingFavoriteController) {
    $result = $clothingFavoriteController->findAll();
    echo json_encode(['result' => $result]);
});


/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Method: POST
 * - Path: /clothingFavorite/createClothingFavorite
 * - Description: Create a new clothing favorite
 * - Parameters: {
 *     - user_id: (Int) user ID
 *     - clothing_id: (Int) clothing ID
 * }
 */
$router->post($url.'/clothingFavorite/createClothingFavorite', function ($params) use ($clothingFavoriteController) {
    $user_id = $params['user_id'] ?? null;
    $clothing_id = $params['clothing_id'] ?? null;

    if ($user_id && $clothing_id) {
        $result = $clothingFavoriteController->createClothingFavorite($user_id, $clothing_id);
        echo json_encode($result);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Method: PUT
 * - Path: /clothingFavorite/updateClothingFavorite
 * - Description: Update a clothing favorite
 * - Parameters: {
 *     - id: (Int) clothing favorite ID
 *     - user_id: (Int) user ID
 *     - clothing_id: (Int) clothing ID
 * }
 */
$router->put($url.'/clothingFavorite/updateClothingFavorite', function ($params) use ($clothingFavoriteController) {
    $id = $params['id'] ?? null;
    $user_id = $params['user_id'] ?? null;
    $clothing_id = $params['clothing_id'] ?? null;

    if ($id && $user_id && $clothing_id) {
        $result = $clothingFavoriteController->updateClothingFavorite($id, $user_id, $clothing_id);
        echo json_encode($result);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Method: DELETE
 * - Path: /clothingFavorite/deleteClothingFavorite
 * - Description: Delete a clothing favorite
 * - Parameters: {
 *     - id: (Int) clothing favorite ID
 * }
 */
$router->delete($url.'/clothingFavorite/deleteClothingFavorite', function ($params) use ($clothingFavoriteController) {
    $id = $params['id'] ?? null;

    if ($id) {
        $clothingFavoriteController->deleteClothingFavorite($id);
        echo json_encode(['result' => 'Clothing favorite supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});

/**
 * @ApiRoute
 * - Groupe: ClothingFavorite
 * - Method: POST
 * - Path: /clothingFavorite/getById
 * - Description: Get a clothing favorite by ID
 * - Parameters: {
 *     - id: (Int) clothing favorite ID
 * }
 */
$router->post($url.'/clothingFavorite/getById', function ($params) use ($clothingFavoriteController) {
    $id = $params['id'] ?? null;

    if ($id) {
        $result = $clothingFavoriteController->getClothingFavoriteById($id);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
