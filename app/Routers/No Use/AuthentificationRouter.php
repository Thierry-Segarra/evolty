<?php
use App\Controllers\UserController;
use App\Models\UserModel;
use EvoltyFramework\Tools\Session;
// Route for getUserById
$userModel = new UserModel();
$userController = new UserController($userModel);
$session = Session::getInstance();

/**
 * @ApiRoute
 * - Groupe: Auth
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

// Route for user authentication and token generation
/**
 * @ApiRoute
 * - Groupe: Auth
 * - Method: POST
 * - Path: /auth/login
 * - Description: User login and token generation
 * - Parameters: {
 *     - username: (String) ex:(mast) username User
 *     - password: (String) ex:(test) password User
 * }
 */
$router->post($url.'/auth/login', function ($params) use ($userController, $session) {

    $username = $params['username'] ?? null;
    $password = $params['password'] ?? null;

    // Vérification des paramètres de connexion
    if ($username && $password) {
        // Vérification des informations d'identification de l'utilisateur
        $userData = $userController->authenticateUser($username, $password);
        
        // Si les informations d'identification sont valides, générer le token d'accès et le refresh token
        if ($userData) {
            // Génération du token d'accès
            $accessToken = $session->generateJWT($userData);

            // Génération du refresh token
            // $refreshTokenData = [
            //     'id' => $userData['user_id'],
            //     'username' => $userData['username']
            //     // Tu peux ajouter d'autres données au refresh token si nécessaire
            // ];
            // $refreshToken = $session->generateRefreshToken($refreshTokenData);

            // Retourner les tokens générés
            echo json_encode([
                'access_token' => $accessToken
            ]);
        } else {
            // Si les informations d'identification sont invalides, retourner une erreur d'authentification
            echo json_encode(['error' => 'Invalid username or password']);
        }
    } else {
        // Si les paramètres de connexion sont manquants, retourner une erreur
        echo json_encode(['error' => 'Missing username or password']);
    }
});



/**
 * @ApiRoute
 * - Groupe: Auth
 * - Method: GET
 * - Path: /auth/testCheck
 * - Description: Vérifier si l'utilisateur est authentifié
 * - Security
 */
$router->get($url.'/auth/testCheck', function () use ($session) {
    $authResult = $session->getSession();
    echo json_encode($authResult);
});