<?php
use App\Controllers\ClothingTypeController;
use App\Models\ClothingTypeModel;

// Instantiate the ClothingTypeModel and ClothingTypeController
$clothingTypeModel = new ClothingTypeModel();
$clothingTypeController = new ClothingTypeController($clothingTypeModel);

/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Description: Get all clothing types
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Method: GET
 * - Path: /clothingType/findAll
 * - Description: Get all clothing types
 */
$router->get($url.'/clothingType/findAll', function ($params) use ($clothingTypeController) {
    $clothingTypeData = $clothingTypeController->findAll();
    echo json_encode(['result' => $clothingTypeData]);
});

// Route for getClothingTypeById
/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Method: POST
 * - Path: /clothingType/getClothingType
 * - Description: Get clothing type by Id
 * - Parameters: {
 *     - id: (Int) id ClothingType
 * }
 */ 
$router->post($url.'/clothingType/getClothingType', function ($params) use ($clothingTypeController) {
    $clothingTypeId = $params['id'];

    if (isset($clothingTypeId)) {
        $clothingTypeData = $clothingTypeController->getClothingTypeById($clothingTypeId);
        echo json_encode(['result' => $clothingTypeData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createClothingType
/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Method: POST
 * - Path: /clothingType/createClothingType
 * - Description: Create a new clothing type
 * - Parameters: {
 *     - name: (String) name
 *     - category: (String) category
 * }
 */
$router->post($url.'/clothingType/createClothingType', function ($params) use ($clothingTypeController) {
    $name = $params['name'] ?? null;
    $category = $params['category'] ?? null;

    if ($name && $category) {
        echo json_encode($clothingTypeController->createClothingType($name, $category));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateClothingType
/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Method: PUT
 * - Path: /clothingType/updateClothingType
 * - Description: Update clothing type by Id
 * - Parameters: {
 *     - id: (Int) clothing type id
 *     - name: (String) name
 *     - category: (String) category
 * }
 */
$router->put($url.'/clothingType/updateClothingType', function ($params) use ($clothingTypeController) {
    $clothingTypeId = $params['id'] ?? null;
    $name = $params['name'] ?? null;
    $category = $params['category'] ?? null;

    if ($clothingTypeId && $name && $category) {
        $result = $clothingTypeController->updateClothingType($clothingTypeId, $name, $category);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteClothingType
/**
 * @ApiRoute
 * - Groupe: ClothingType
 * - Method: DELETE
 * - Path: /clothingType/deleteClothingType
 * - Description: Delete clothing type by Id
 * - Parameters: {
 *     - id: (Int) clothing type id
 * }
 */
$router->delete($url.'/clothingType/deleteClothingType', function ($params) use ($clothingTypeController) {
    $clothingTypeId = $params['id'] ?? null;

    if ($clothingTypeId) {
        $clothingTypeController->deleteClothingType($clothingTypeId);
        echo json_encode(['result' => 'Clothing type supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
