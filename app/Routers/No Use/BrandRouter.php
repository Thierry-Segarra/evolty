<?php
use App\Controllers\BrandController;
use App\Models\BrandModel;

// Instantiate the BrandModel and BrandController
$brandModel = new BrandModel();
$brandController = new BrandController($brandModel);

/**
 * @ApiRoute
 * - Groupe: Brand
 * - Description: Get all brands
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: Brand
 * - Method: GET
 * - Path: /brand/findAll
 * - Description: Get all brands
 */
$router->get($url.'/brand/findAll', function ($params) use ($brandController) {
    $brandData = $brandController->findAll();
    echo json_encode(['result' => $brandData]);
});

// Route for getBrandById
/**
 * @ApiRoute
 * - Groupe: Brand
 * - Method: POST
 * - Path: /brand/getBrand
 * - Description: Get brand by Id
 * - Parameters: {
 *     - id: (Int) id Brand
 * }
 */ 
$router->post($url.'/brand/getBrand', function ($params) use ($brandController) {
    $brandId = $params['id'];

    if (isset($brandId)) {
        $brandData = $brandController->getBrandById($brandId);
        echo json_encode(['result' => $brandData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createBrand
/**
 * @ApiRoute
 * - Groupe: Brand
 * - Method: POST
 * - Path: /brand/createBrand
 * - Description: Create a new brand
 * - Parameters: {
 *     - name: (String) brand name
 * }
 */
$router->post($url.'/brand/createBrand', function ($params) use ($brandController) {
    $brandName = $params['name'] ?? null;

    if ($brandName) {
        echo json_encode($brandController->createBrand($brandName));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre manquant']);
    }
});

// Route for updateBrand
/**
 * @ApiRoute
 * - Groupe: Brand
 * - Method: PUT
 * - Path: /brand/updateBrand
 * - Description: Update brand by Id
 * - Parameters: {
 *     - id: (Int) brand id
 *     - name: (String) brand name
 * }
 */
$router->put($url.'/brand/updateBrand', function ($params) use ($brandController) {
    $brandId = $params['id'] ?? null;
    $brandName = $params['name'] ?? null;

    if ($brandId && $brandName) {
        $result = $brandController->updateBrand($brandId, $brandName);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteBrand
/**
 * @ApiRoute
 * - Groupe: Brand
 * - Method: DELETE
 * - Path: /brand/deleteBrand
 * - Description: Delete brand by Id
 * - Parameters: {
 *     - id: (Int) brand id
 * }
 */
$router->delete($url.'/brand/deleteBrand', function ($params) use ($brandController) {
    $brandId = $params['id'] ?? null;

    if ($brandId) {
        $brandController->deleteBrand($brandId);
        echo json_encode(['result' => 'Brand supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
