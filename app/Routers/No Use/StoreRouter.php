<?php
use App\Controllers\StoreController;
use App\Models\StoreModel;

// Instantiate the StoreModel and StoreController
$storeModel = new StoreModel();
$storeController = new StoreController($storeModel);

/**
 * @ApiRoute
 * - Groupe: Store
 * - Description: Get all stores
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: Store
 * - Method: GET
 * - Path: /store/findAll
 * - Description: Get all stores
 */
$router->get($url.'/store/findAll', function ($params) use ($storeController) {
    $storeData = $storeController->findAll();
    echo json_encode(['result' => $storeData]);
});

// Route for getStoreById
/**
 * @ApiRoute
 * - Groupe: Store
 * - Method: POST
 * - Path: /store/getStore
 * - Description: Get store by Id
 * - Parameters: {
 *     - id: (Int) id Store
 * }
 */ 
$router->post($url.'/store/getStore', function ($params) use ($storeController) {
    $storeId = $params['id'];

    if (isset($storeId)) {
        $storeData = $storeController->getStoreById($storeId);
        echo json_encode(['result' => $storeData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createStore
/**
 * @ApiRoute
 * - Groupe: Store
 * - Method: POST
 * - Path: /store/createStore
 * - Description: Create a new store
 * - Parameters: {
 *     - name: (String) store name
 *     - address: (String) store address
 *     - phone: (String) store phone number
 *     - email: (String) store email
 * }
 */
$router->post($url.'/store/createStore', function ($params) use ($storeController) {
    $name = $params['name'] ?? null;
    $address = $params['address'] ?? null;
    $phone = $params['phone'] ?? null;
    $email = $params['email'] ?? null;

    if ($name && $address && $phone && $email) {
        echo json_encode($storeController->createStore($name, $address, $phone, $email));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateStore
/**
 * @ApiRoute
 * - Groupe: Store
 * - Method: PUT
 * - Path: /store/updateStore
 * - Description: Update store by Id
 * - Parameters: {
 *     - id: (Int) store id
 *     - name: (String) store name
 *     - address: (String) store address
 *     - phone: (String) store phone number
 *     - email: (String) store email
 * }
 */
$router->put($url.'/store/updateStore', function ($params) use ($storeController) {
    $storeId = $params['id'] ?? null;
    $name = $params['name'] ?? null;
    $address = $params['address'] ?? null;
    $phone = $params['phone'] ?? null;
    $email = $params['email'] ?? null;

    if ($storeId && $name && $address && $phone && $email) {
        $result = $storeController->updateStore($storeId, $name, $address, $phone, $email);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteStore
/**
 * @ApiRoute
 * - Groupe: Store
 * - Method: DELETE
 * - Path: /store/deleteStore
 * - Description: Delete store by Id
 * - Parameters: {
 *     - id: (Int) store id
 * }
 */
$router->delete($url.'/store/deleteStore', function ($params) use ($storeController) {
    $storeId = $params['id'] ?? null;

    if ($storeId) {
        $storeController->deleteStore($storeId);
        echo json_encode(['result' => 'Store supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
