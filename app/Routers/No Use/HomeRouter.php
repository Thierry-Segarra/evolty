<?php
use EvoltyFramework\Tools\Log;
/**
 * @Route
 * - Method: GET
 * - Path: /
 */
$router->get('/', function ($params) use ($router){
    $view = __DIR__ . '/../Views/home.php';
    // Adding a message parameter to pass to the view
    $params['message'] = "Hello, this is a parameter from the backend!";
    
    $router->renderView($view, $params);
});


/**
 * @Route
 * - Method: POST
 * - Path: /
 */
$router->post('/', function ($params) use ($router) {
    // Process the form input
    $input = isset($_POST['input']) ? $_POST['input'] : '';
    
    // Pass the result to the view
    $params['result'] = "You entered: " . $input;

    $log = new Log();

    $log->addArrayLog($params);
    
    $view = __DIR__ . '/../Views/home.php';
    $router->renderView($view, $params);
});


?>