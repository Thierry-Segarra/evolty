<?php
use App\Controllers\UserController;
use App\Models\UserModel;

use EvoltyFramework\Tools\Log;
// Route for getUserById
$userModel = new UserModel();
$userController = new UserController($userModel);

/**
 * @ApiRoute
 * - Groupe: User
 * - Description: Get all user
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});


/**
 * @ApiRoute
 * - Groupe: User
 * - Method: GET
 * - Path: /user/findAll
 * - Description: Get all user
 */
$router->get($url.'/user/findAll', function ($params) use ($userController){
    $userData = $userController->findAll();
    echo json_encode(['result' => $userData]);
});

// Route for getUserById
/**
 * @ApiRoute
 * - Groupe: User
 * - Method: POST
 * - Path: /user/getUser
 * - Description: Get user by Id
 * - Parameters: {
 *     - id: (Int) id User
 * }
 */ 
$router->post($url.'/user/getUser', function ($params) use ($userController){
    $userId = $params['id'];

    if (isset($userId) && $userId != null) {
        $userData = $userController->getUserById($userId);
        echo json_encode(['result' => $userData]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'element vide']);
    }
});


// Route for createUser
/**
 * @ApiRoute
 * - Groupe: User
 * - Method: POST
 * - Path: /user/createUser
 * - Description: Get user by Id
 * - Parameters: {
 *     - username: (String) username User
 *     - email: (String) email User
 *     - password: (String) password User
 *     - gender: (String) gender User
 *     - birth_date: (Date) birth_date User
 *     - role_id: (Int) role User
 * }
 */
$router->post($url.'/user/createUser', function ($params) use ($userController) {
    $log = new Log();
    $log->addArrayLog($params);
    $username = $params['username'] ?? null;
    $email = $params['email'] ?? null;
    $password = $params['password'] ?? null;
    $gender = $params['gender'] ?? null;
    $birth_date = $params['birth_date'] ?? null;
    $role_id = $params['role_id'] ?? null;

    if ($username && $email && $password && $gender && $birth_date && $role_id) {
        echo json_encode($userController->createUser($username, $email, $password, $gender, $birth_date, $role_id));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateUser
/**
 * @ApiRoute
 * - Groupe: User
 * - Method: PUT
 * - Path: /user/updateUser
 * - Description: Update user by Id
 * - Parameters: {
 *     - id: (String) id User
 *     - username: (String) username User
 *     - email: (String) email User
 * }
 */
$router->put($url.'/user/updateUser', function ($params) use ($userController) {
    $userId = $params['id'] ?? null;
    $username = $params['username'] ?? null;
    $email = $params['email'] ?? null;

    if ($userId && $username && $email) {
        $result = $userController->updateUser($userId, $username, $email);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteUser
/**
 * @ApiRoute
 * - Groupe: User
 * - Method: DELETE
 * - Path: /user/deleteUser
 * - Description: Delete user by Id
 * - Parameters: {
 *     - id: (String) id User
 * }
 */
$router->delete($url.'/user/deleteUser', function ($params) use ($userController) {
    $userId = $params['id'] ?? null;

    if ($userId) {
        $userController->deleteUser($userId);
        echo json_encode(['result' => 'Utilisateur supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});
