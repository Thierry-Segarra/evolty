<?php
use App\Controllers\UserRoleController;
use App\Models\UserRoleModel;

// Instantiate the UserRoleModel and UserRoleController
$userRoleModel = new UserRoleModel();
$userRoleController = new UserRoleController($userRoleModel);

/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Description: Get all user roles
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Method: GET
 * - Path: /userRole/findAll
 * - Description: Get all user roles
 */
$router->get('/userRole/findAll', function ($params) use ($userRoleController){
    $userRoleData = $userRoleController->findAll();
    echo json_encode(['result' => $userRoleData]);
});

// Route for getUserRoleById
/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Method: POST
 * - Path: /userRole/getUserRole
 * - Description: Get user role by Id
 * - Parameters: {
 *     - id: (Int) id UserRole
 * }
 */ 
$router->post('/userRole/getUserRole', function ($params) use ($userRoleController){
    $userRoleId = $params['id'];
    
    if (isset($userRoleId) && $userRoleId != null ) {
        $userRoleData = $userRoleController->getUserRoleById($userRoleId);
        echo json_encode(['result' => $userRoleData]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for createUserRole
/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Method: POST
 * - Path: /userRole/createUserRole
 * - Description: Create a new user role
 * - Parameters: {
 *     - name: (String) name UserRole
 * }
 */
$router->post('/userRole/createUserRole', function ($params) use ($userRoleController) {
    $name = $params['name'] ?? null;

    if ($name) {
        echo json_encode($userRoleController->createUserRole($name));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateUserRole
/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Method: PUT
 * - Path: /userRole/updateUserRole
 * - Description: Update user role by Id
 * - Parameters: {
 *     - id: (Int) id UserRole
 *     - name: (String) name UserRole
 * }
 */
$router->put('/userRole/updateUserRole', function ($params) use ($userRoleController) {
    $userRoleId = $params['id'] ?? null;
    $name = $params['name'] ?? null;

    if ($userRoleId && $name) {
        $result = $userRoleController->updateUserRole($userRoleId, $name);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteUserRole
/**
 * @ApiRoute
 * - Groupe: UserRole
 * - Method: DELETE
 * - Path: /userRole/deleteUserRole
 * - Description: Delete user role by Id
 * - Parameters: {
 *     - id: (Int) id UserRole
 * }
 */
$router->delete('/userRole/deleteUserRole', function ($params) use ($userRoleController) {
    $userRoleId = $params['id'] ?? null;

    if ($userRoleId) {
        $userRoleController->deleteUserRole($userRoleId);
        echo json_encode(['result' => 'Rôle utilisateur supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
