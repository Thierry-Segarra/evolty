<?php
use App\Controllers\StoreClothingController;
use App\Models\StoreClothingModel;

// Instantiate the StoreClothingModel and StoreClothingController
$storeClothingModel = new StoreClothingModel();
$storeClothingController = new StoreClothingController($storeClothingModel);

/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Description: Get all store clothings
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Method: GET
 * - Path: /storeClothing/findAll
 * - Description: Get all store clothings
 */
$router->get('/storeClothing/findAll', function ($params) use ($storeClothingController) {
    $storeClothingData = $storeClothingController->findAll();
    echo json_encode(['result' => $storeClothingData]);
});

// Route for getStoreClothingById
/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Method: POST
 * - Path: /storeClothing/getStoreClothing
 * - Description: Get store clothing by Id
 * - Parameters: {
 *     - id: (Int) id StoreClothing
 * }
 */ 
$router->post('/storeClothing/getStoreClothing', function ($params) use ($storeClothingController) {
    $storeClothingId = $params['id'];

    if (isset($storeClothingId)) {
        $storeClothingData = $storeClothingController->getStoreClothingById($storeClothingId);
        echo json_encode(['result' => $storeClothingData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createStoreClothing
/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Method: POST
 * - Path: /storeClothing/createStoreClothing
 * - Description: Create a new store clothing
 * - Parameters: {
 *     - store_id: (Int) store id
 *     - clothing_id: (Int) clothing id
 * }
 */
$router->post('/storeClothing/createStoreClothing', function ($params) use ($storeClothingController) {
    $storeId = $params['store_id'] ?? null;
    $clothingId = $params['clothing_id'] ?? null;

    if ($storeId && $clothingId) {
        echo json_encode($storeClothingController->createStoreClothing($storeId, $clothingId));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateStoreClothing
/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Method: PUT
 * - Path: /storeClothing/updateStoreClothing
 * - Description: Update store clothing by Id
 * - Parameters: {
 *     - id: (Int) store clothing id
 *     - store_id: (Int) store id
 *     - clothing_id: (Int) clothing id
 * }
 */
$router->put('/storeClothing/updateStoreClothing', function ($params) use ($storeClothingController) {
    $storeClothingId = $params['id'] ?? null;
    $storeId = $params['store_id'] ?? null;
    $clothingId = $params['clothing_id'] ?? null;

    if ($storeClothingId && $storeId && $clothingId) {
        $result = $storeClothingController->updateStoreClothing($storeClothingId, $storeId, $clothingId);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteStoreClothing
/**
 * @ApiRoute
 * - Groupe: StoreClothing
 * - Method: DELETE
 * - Path: /storeClothing/deleteStoreClothing
 * - Description: Delete store clothing by Id
 * - Parameters: {
 *     - id: (Int) store clothing id
 * }
 */
$router->delete('/storeClothing/deleteStoreClothing', function ($params) use ($storeClothingController) {
    $storeClothingId = $params['id'] ?? null;

    if ($storeClothingId) {
        $storeClothingController->deleteStoreClothing($storeClothingId);
        echo json_encode(['result' => 'Store clothing supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
