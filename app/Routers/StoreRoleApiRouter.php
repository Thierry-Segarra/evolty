<?php
use App\Controllers\StoreRoleController;
use App\Models\StoreRoleModel;

// Instantiate the StoreRoleModel and StoreRoleController
$storeRoleModel = new StoreRoleModel();
$storeRoleController = new StoreRoleController($storeRoleModel);

/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Description: Get all store roles
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Method: GET
 * - Path: /storeRole/findAll
 * - Description: Get all store roles
 */
$router->get('/storeRole/findAll', function ($params) use ($storeRoleController){
    $storeRoleData = $storeRoleController->findAll();
    echo json_encode(['result' => $storeRoleData]);
});

// Route for getStoreRoleById
/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Method: POST
 * - Path: /storeRole/getStoreRole
 * - Description: Get store role by Id
 * - Parameters: {
 *     - id: (Int) id StoreRole
 * }
 */ 
$router->post('/storeRole/getStoreRole', function ($params) use ($storeRoleController){
    $storeRoleId = $params['id'];

    if (isset($storeRoleId)) {
        $storeRoleData = $storeRoleController->getStoreRoleById($storeRoleId);
        echo json_encode(['result' => $storeRoleData]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for createStoreRole
/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Method: POST
 * - Path: /storeRole/createStoreRole
 * - Description: Create a new store role
 * - Parameters: {
 *     - name: (String) name StoreRole
 * }
 */
$router->post('/storeRole/createStoreRole', function ($params) use ($storeRoleController) {
    $name = $params['name'] ?? null;

    if ($name) {
        echo json_encode($storeRoleController->createStoreRole($name));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateStoreRole
/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Method: PUT
 * - Path: /storeRole/updateStoreRole
 * - Description: Update store role by Id
 * - Parameters: {
 *     - id: (Int) id StoreRole
 *     - name: (String) name StoreRole
 * }
 */
$router->put('/storeRole/updateStoreRole', function ($params) use ($storeRoleController) {
    $storeRoleId = $params['id'] ?? null;
    $name = $params['name'] ?? null;

    if ($storeRoleId && $name) {
        $result = $storeRoleController->updateStoreRole($storeRoleId, $name);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteStoreRole
/**
 * @ApiRoute
 * - Groupe: StoreRole
 * - Method: DELETE
 * - Path: /storeRole/deleteStoreRole
 * - Description: Delete store role by Id
 * - Parameters: {
 *     - id: (Int) id StoreRole
 * }
 */
$router->delete('/storeRole/deleteStoreRole', function ($params) use ($storeRoleController) {
    $storeRoleId = $params['id'] ?? null;

    if ($storeRoleId) {
        $storeRoleController->deleteStoreRole($storeRoleId);
        echo json_encode(['result' => 'Rôle magasin supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});
