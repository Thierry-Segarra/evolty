<?php
use App\Controllers\ClothingStyleController;
use App\Models\ClothingStyleModel;

// Instantiate the ClothingStyleModel and ClothingStyleController
$clothingStyleModel = new ClothingStyleModel();
$clothingStyleController = new ClothingStyleController($clothingStyleModel);

/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Description: Get all clothing styles
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Method: GET
 * - Path: /clothingStyle/findAll
 * - Description: Get all clothing styles
 */
$router->get('/clothingStyle/findAll', function ($params) use ($clothingStyleController) {
    $clothingStyleData = $clothingStyleController->findAll();
    echo json_encode(['result' => $clothingStyleData]);
});

// Route for getClothingStyleById
/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Method: POST
 * - Path: /clothingStyle/getClothingStyle
 * - Description: Get clothing style by Id
 * - Parameters: {
 *     - id: (Int) id ClothingStyle
 * }
 */ 
$router->post('/clothingStyle/getClothingStyle', function ($params) use ($clothingStyleController) {
    $clothingStyleId = $params['id'];

    if (isset($clothingStyleId)) {
        $clothingStyleData = $clothingStyleController->getClothingStyleById($clothingStyleId);
        echo json_encode(['result' => $clothingStyleData]);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});

// Route for createClothingStyle
/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Method: POST
 * - Path: /clothingStyle/createClothingStyle
 * - Description: Create a new clothing style
 * - Parameters: {
 *     - name: (String) name
 * }
 */
$router->post('/clothingStyle/createClothingStyle', function ($params) use ($clothingStyleController) {
    $name = $params['name'] ?? null;

    if ($name) {
        echo json_encode($clothingStyleController->createClothingStyle($name));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre name manquant']);
    }
});

// Route for updateClothingStyle
/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Method: PUT
 * - Path: /clothingStyle/updateClothingStyle
 * - Description: Update clothing style by Id
 * - Parameters: {
 *     - id: (Int) clothing style id
 *     - name: (String) name
 * }
 */
$router->put('/clothingStyle/updateClothingStyle', function ($params) use ($clothingStyleController) {
    $clothingStyleId = $params['id'] ?? null;
    $name = $params['name'] ?? null;

    if ($clothingStyleId && $name) {
        $result = $clothingStyleController->updateClothingStyle($clothingStyleId, $name);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteClothingStyle
/**
 * @ApiRoute
 * - Groupe: ClothingStyle
 * - Method: DELETE
 * - Path: /clothingStyle/deleteClothingStyle
 * - Description: Delete clothing style by Id
 * - Parameters: {
 *     - id: (Int) clothing style id
 * }
 */
$router->delete('/clothingStyle/deleteClothingStyle', function ($params) use ($clothingStyleController) {
    $clothingStyleId = $params['id'] ?? null;

    if ($clothingStyleId) {
        $clothingStyleController->deleteClothingStyle($clothingStyleId);
        echo json_encode(['result' => 'Clothing style supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètre ID manquant']);
    }
});
