<?php
$projectRoot = dirname(__DIR__, 2);

// Liste Routers

require_once($projectRoot.'/app/Routers/HomeApiRouter.php');
// Ia
require_once($projectRoot.'/app/Routers/IaApiRouter.php');
// User
require_once($projectRoot.'/app/Routers/UserApiRouter.php');
require_once($projectRoot.'/app/Routers/UserRoleApiRouter.php');

// Clothing
require_once($projectRoot.'/app/Routers/ClothingApiRouter.php');
require_once($projectRoot.'/app/Routers/ClothingStyleApiRouter.php');
require_once($projectRoot.'/app/Routers/ClothingTypeApiRouter.php');
require_once($projectRoot.'/app/Routers/ClothingFavoriteApiRouter.php');
require_once($projectRoot.'/app/Routers/UserClothingApiRouter.php');


require_once($projectRoot.'/app/Routers/BrandApiRouter.php');
require_once($projectRoot.'/app/Routers/StoreApiRouter.php');
require_once($projectRoot.'/app/Routers/StoreClothingApiRouter.php');
require_once($projectRoot.'/app/Routers/StoreManagerApiRouter.php');