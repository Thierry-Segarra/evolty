<?php
use App\Controllers\StoreManagerController;
use App\Models\StoreManagerModel;

// Instantiate the StoreManagerModel and StoreManagerController
$storeManagerModel = new StoreManagerModel();
$storeManagerController = new StoreManagerController($storeManagerModel);

/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Description: Get all store managers
 */
$router->addGroupe(function () {
    // Cette fonction n'a pas besoin de faire quelque chose, elle est juste utilisée pour l'annotation
});

/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Method: GET
 * - Path: /storeManager/findAll
 * - Description: Get all store managers
 */
$router->get('/storeManager/findAll', function ($params) use ($storeManagerController) {
    $storeManagerData = $storeManagerController->findAll();
    echo json_encode(['result' => $storeManagerData]);
});

// Route for getStoreManagerById
/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Method: POST
 * - Path: /storeManager/getStoreManager
 * - Description: Get store manager by Id
 * - Parameters: {
 *     - id: (Int) id StoreManager
 * }
 */ 
$router->post('/storeManager/getStoreManager', function ($params) use ($storeManagerController){
    $storeManagerId = $params['id'];

    if (isset($storeManagerId)) {
        $storeManagerData = $storeManagerController->getStoreManagerById($storeManagerId);
        echo json_encode(['result' => $storeManagerData]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for createStoreManager
/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Method: POST
 * - Path: /storeManager/createStoreManager
 * - Description: Create a new store manager
 * - Parameters: {
 *     - store_id: (Int) store_id
 *     - user_id: (Int) user_id
 *     - role_id: (Int) role_id
 * }
 */
$router->post('/storeManager/createStoreManager', function ($params) use ($storeManagerController) {
    $store_id = $params['store_id'] ?? null;
    $user_id = $params['user_id'] ?? null;
    $role_id = $params['role_id'] ?? null;

    if ($store_id && $user_id && $role_id) {
        echo json_encode($storeManagerController->createStoreManager($store_id, $user_id, $role_id));
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for updateStoreManager
/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Method: PUT
 * - Path: /storeManager/updateStoreManager
 * - Description: Update store manager by Id
 * - Parameters: {
 *     - id: (Int) id StoreManager
 *     - store_id: (Int) store_id
 *     - user_id: (Int) user_id
 *     - role_id: (Int) role_id
 * }
 */
$router->put('/storeManager/updateStoreManager', function ($params) use ($storeManagerController) {
    $storeManagerId = $params['id'] ?? null;
    $store_id = $params['store_id'] ?? null;
    $user_id = $params['user_id'] ?? null;
    $role_id = $params['role_id'] ?? null;

    if ($storeManagerId && $store_id && $user_id && $role_id) {
        $result = $storeManagerController->updateStoreManager($storeManagerId, $store_id, $user_id, $role_id);
        echo json_encode(['result' => $result]);
    } else {
        http_response_code(400);
        echo json_encode(['erreur' => 'Paramètres manquants']);
    }
});

// Route for deleteStoreManager
/**
 * @ApiRoute
 * - Groupe: StoreManager
 * - Method: DELETE
 * - Path: /storeManager/deleteStoreManager
 * - Description: Delete store manager by Id
 * - Parameters: {
 *     - id: (Int) id StoreManager
 * }
 */
$router->delete('/storeManager/deleteStoreManager', function ($params) use ($storeManagerController) {
    $storeManagerId = $params['id'] ?? null;

    if ($storeManagerId) {
        $storeManagerController->deleteStoreManager($storeManagerId);
        echo json_encode(['result' => 'Gestionnaire de magasin supprimé avec succès']);
    } else {
        http_response_code(400);
        echo json_encode(['result' => 'Paramètre ID manquant']);
    }
});
