<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
<h1>Welcome to the Home Page</h1>
    <!-- Display the message parameter from the backend -->
    <?php if (isset($params['message'])): ?>
        <p><?php echo htmlspecialchars($params['message']); ?></p>
    <?php endif; ?>

    
    <!-- Display the result from form submission if exists -->
    <?php if (isset($params['result'])): ?>
        <p>Result: <?php echo htmlspecialchars($params['result']); ?></p>
    <?php endif; ?>

    <!-- Simple form to submit data -->
    <form method="post" action="/">
        <label for="input">Enter something:</label>
        <input type="text" id="input" name="input" required>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
